# Plan For DancingDroid'huh

## Etape 1

## Version 0.1.0 Déplacements et collisions fonctionnelles
### Bibi:
- Écrire une `enum Orientation` qui gére les 4 cas possibles
- Écrire une fonction qui lit un caractères `char` et crée une `Orientation`
- Écrire un [commentaire de documentation](https://doc.rust-lang.org/stable/rust-by-example/meta/doc.html) de cette fonction qui dit comment l'utiliser
- Écrire des [tests](https://doc.rust-lang.org/book/ch11-01-writing-tests.html) pour vérifer que votre fonction fait ce que vous voulez.

## Deplacement orientation

Dans ce jeu des robots vont pouvoir se déplacer dans un espace en deux
dimensions. A chaque tour un robot executera un ordre les ordres concistent en
effectuer une rotation à droite ou à gauche ou avancer ou ne rien faire!

> Conseil: Deux type `enum` vous serons utile! ;) Une pour l'orientation, une pour
> representer les instructions!

Dans le fichier d'instructions vous aurrez les caractères suivants possibles:

- `L`: Tourne le robot à gauche par exemple passe de orientation: `N` à `W`
- `R`: Tourne le robot à droite par exemple passe de orientation: `N` à `E`
- `F`: Avance dans le sens de l'orientation d'une case

## Collisions

En cas de collision avec un autre robot lors d'un déplacement le robot devra
dire sur la sortie standard:

Cas des collisions: Faire dire `"Robot ID<numId> Collision en (x, y)"` et ne
pas comptabiliser le mouvement du Robot qui occassionne la collision, mais
consommer son instruction.

Format du fichier definisant le monde:

**Important** : Dans le format de fichier la gestion des commentaires `\\` est optionnelle .
```txt
5 5   // X_max Y_max
1 1 N // position du robot en x=1 y=1 orientation = nord
FLLFRF

3 2 S // position du robot 2 x=3 y=2 orientation=South
FFLFRRF
```

Pour representer:

- un robot une simple structure suffira.
- La structure pour contenir les robots pourras être un `Vec<Robot>`

### Tests et Documentation

Écrire des tests de votre code comme indiqué dans cette partie du livre
[ch11 tests](https://doc.rust-lang.org/book/ch11-01-writing-tests.html) ou de [Rust By Example](https://doc.rust-lang.org/rust-by-example/testing.html)


### Conclusion version 0.1.0

Dans certe première itération, vous devrez pouvoir faire la simulation des robots,
depuis un fichier d'instruction, et afficher a la fin leurs positions finales.


## Version 0.2.0 Affichage implementation de Display
### Quentin:
Dans cette version on va écrire du code pour afficher la positions des robots et
dessiner la grille.

Vous devrez pouvoir dessiner quelque chose comme cela en partant de la grille
donnée en exemple plus haut dans votre terminal:

```txt
Terrain { x_max = 5; y_max = 5 }
Robots [
 { id = 0, x = 1; y = 1; orientation: North, instructions: [F,L,L,F,R,F], },
 { id = 1; x = 3; y = 2; orientation: South, instructions: [F,F,L,F,R,R,F], },
]
Etat initial
============================
5 .  .  .  .  .  .
4 .  .  .  .  .  .
3 .  .  .  .  .  .
2 .  .  .  ⬇  .  .
1 .  ⬆  .  .  .  .
0 .  .  .  .  .  .
  0  1  2  3  4  5
Etat final
=============================
5 .  .  .  .  .  .
4 .  .  .  .  .  .
3 .  .  .  .  .  .
2 .  .  .  .  .  .
1 ⬅  .  .  .  .  .
0 .  .  .  ⬅  .  .
  0  1  2  3  4  5
```

Pour réaliser cela on peut implementer un trait pour nos structures qui s'appelle
`Display` c'est un peu comme un contrat, qui garanti que si une structure,
l'implemente alors on peut avoir une representation humaine sous forme de `String`.

Vous pouvez implementer aussi #[test] pour vous faciliter la lecture en debuggant
votre code.
