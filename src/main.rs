//* Les lib imports
use colored::*;
use rand::Rng;
use std::collections::HashSet;
use {std::collections::HashMap, std::fmt, std::fs, std::str};
use std::process;
use std::io;

/// Les 4 orientation possibles d'un robot
#[derive(PartialEq, Debug)]
enum OrientationType {
    Nord,
    Sud,
    Ouest,
    Est,
}
/// Les differents type d'instructions executables d'un robot
#[derive(Debug, PartialEq)]
enum InstructionType {
    Gauche,
    Droite,
    Avancer,
}
/// Des coordonnées/Un point
#[derive(PartialEq, Hash, Eq, Copy, Clone, Debug)]
struct CoordType {
    x: i32,
    y: i32,
}
impl CoordType {
    /// Prend des coordonnées et les remets entre 0.0 et max_x.max_y
    fn clamp(&mut self, max: &Self) {
        if self.x < 0 {
            self.x = 0;
        }
        if self.y < 0 {
            self.y = 0;
        }
        if self.x > max.x {
            self.x = max.x;
        }
        if self.y > max.y {
            self.y = max.y;
        }
    }
    fn new(x: i32, y: i32) -> Self {
        let new = CoordType { x, y };
        return new;
    }
}

impl fmt::Display for CoordType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Coord {} {}", self.x,self.y)
    }
}
/// Definition d'une couleur
#[derive(PartialEq, Hash, Eq, Copy, Clone)]
struct ColorType {
    r: u8,
    g: u8,
    b: u8,
}

impl ColorType {
    fn new(r: u8, g: u8, b: u8) -> Self {
        return ColorType { r, g, b };
    }
}
impl fmt::Display for RobotType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.vivant {
            match self.orientation {
                OrientationType::Nord => write!(
                    f,
                    "{}",
                    " ⬆  ".truecolor(self.color.r, self.color.g, self.color.b)
                ),
                OrientationType::Sud => write!(
                    f,
                    "{}",
                    " ⬇  ".truecolor(self.color.r, self.color.g, self.color.b)
                ),
                OrientationType::Ouest => write!(
                    f,
                    "{}",
                    " ⬅  ".truecolor(self.color.r, self.color.g, self.color.b)
                ),
                OrientationType::Est => write!(
                    f,
                    "{}",
                    " ➡  ".truecolor(self.color.r, self.color.g, self.color.b)
                ),
            }
        } else {
            write!(
                f,
                "{}",
                "[💀]".truecolor(self.color.r, self.color.g, self.color.b)
            )
        }
    }
}
/// Definition d'un robot et tout ce qui est neccessaire a son bon fonctionnement
#[derive(PartialEq)]
struct RobotType {
    coord: CoordType,
    orientation: OrientationType,
    instructions: Vec<InstructionType>,
    //? Passer a une queue! pour instructions ?
    vivant: bool,
    color: ColorType,
}

impl RobotType {
    fn new() -> Self {
        return RobotType {
            orientation: OrientationType::Nord,
            instructions: Vec::new(),
            vivant: false,
            coord: CoordType::new(0, 0),
            color: ColorType { r: 0, g: 0, b: 0 },
        };
    }

    fn robot_print(&self, id: &u32) {
        println!(
            "{}",
            "/====================================/".truecolor(
                self.color.r,
                self.color.g,
                self.color.b
            )
        );
        println!(
            "{} : {}\n{}: {} {}: {}",
            "ID".truecolor(self.color.r, self.color.g, self.color.b),
            id,
            "X".truecolor(self.color.r, self.color.g, self.color.b),
            self.coord.x,
            "Y".truecolor(self.color.r, self.color.g, self.color.b),
            self.coord.y
        );
        println!(
            "{}{:?}",
            "Orientation :".truecolor(self.color.r, self.color.g, self.color.b),
            self.orientation
        );
        println!(
            "{}{:?}",
            "Instruction :".truecolor(self.color.r, self.color.g, self.color.b),
            self.instructions
        );
    }
 
    /// Prends un string par exemple 1 1 NFLLRRFFLLR et crée un robot et renvoie le robot et ses coordonnées
    fn initiat(
        instructions: &str,
        id: &u32,
        coord_max: &CoordType,
    ) -> Result<(RobotType, CoordType), String> {
        let mut robot = RobotType::new();
        //*PARSING PARTIE 2 RobotType

        let mut instructions = instructions.split_whitespace();
        let mut robot_coord = CoordType::new(
            //*Parsing de x max
            match match instructions.next() {
                Some(x) => x,
                _ => return Err("Bug Impossible ?".to_string()),
            }
            .parse::<i32>()
            {
                Ok(x) => x,
                _ => return Err("Echec de l`analyse de la coordonnée X d`un robot".to_string()),
            },
            //*Parsing de y max
            match match instructions.next() {
                Some(y) => y,
                _ => return Err("Bug Impossible ?".to_string()),
            }
            .parse::<i32>()
            {
                Ok(y) => y,
                _ => return Err("Echec de l`analyse de la coordonnée Y d`un robot".to_string()),
            },
        );
        robot_coord.clamp(&coord_max);

        //Crée un &str sur les instructions+orientation Exemple:"FFRRLLR"
        let mut instructions = instructions
            .next()
            .ok_or("L`un des robots n`a que des coordonnée")?
            //TODO Detail quel robot ?
            .chars();

        //Place l`orientation dans le robot
        match instructions
            .next()
            .ok_or("Un Robot sans instruction ni orientation de base ?")?
            //TODO Detail quel robot ?
        {
            'N' => robot.orientation = OrientationType::Nord,
            'S' => robot.orientation = OrientationType::Sud,
            'E' => robot.orientation = OrientationType::Est,
            'O' => robot.orientation = OrientationType::Ouest,

            _ => {
                return Err("L`orientation du robot N°ID n`est pas valide"
                    .replace("ID", &id.to_string())
                    .to_string())
            }
        }

        //Place toutes les instructions dans le robot
        for instruct in instructions {
            match instruct {
                'F' => robot.instructions.push(InstructionType::Avancer),
                'L' => robot.instructions.push(InstructionType::Gauche),
                'R' => robot.instructions.push(InstructionType::Droite),
                _ => (), //Ignore les instructions non valide
            }
        }
        robot.vivant = true;
        robot.coord = robot_coord;
        
        Ok((robot, robot_coord))
    }
    ///Prends un robot le fait tourner a gauche
    fn gauche(&mut self) {
        match self.orientation {
            OrientationType::Nord => self.orientation = OrientationType::Ouest,
            OrientationType::Ouest => self.orientation = OrientationType::Sud,
            OrientationType::Sud => self.orientation = OrientationType::Est,
            OrientationType::Est => self.orientation = OrientationType::Nord,
        }
    }
    /// Prends un robot le fait tourner a droite
    fn droite(&mut self) {
        match self.orientation {
            OrientationType::Nord => self.orientation = OrientationType::Est,
            OrientationType::Ouest => self.orientation = OrientationType::Nord,
            OrientationType::Sud => self.orientation = OrientationType::Ouest,
            OrientationType::Est => self.orientation = OrientationType::Sud,
        }
    }
    /// Prends un robot renvoie des coordonnée avancée selon la l'orientation de celui-ci
    fn avancer(&self) -> CoordType {
        let mut coord = self.coord;
        match self.orientation {
            OrientationType::Nord => coord.y += 1,
            OrientationType::Ouest => coord.x -= 1,
            OrientationType::Sud => coord.y -= 1,
            OrientationType::Est => coord.x += 1,
        }
        return coord;
    }
}
///Tout les données neccessaire pour un monde fonctionnel
struct WorldType {
    coord_max: CoordType,
    all_robots: HashMap<u32, RobotType>,
    // Pour de la rapidité
    all_robots_coords: HashMap<CoordType, u32>,
    colored_case: Vec<HashMap<CoordType, u32>>,
    used_color: HashMap<ColorType, u32>,
}

impl WorldType {
    fn new() -> WorldType {
        return WorldType {
            all_robots: HashMap::new(),
            coord_max: CoordType { x: 0, y: 0 },
            colored_case: Vec::new(),
            all_robots_coords: HashMap::new(),
            used_color: HashMap::new(),
        };
    }
    /// Crée un monde a partir du string en format "Xmax Ymax\nRobotx\n"
    fn initiate(input: String) -> Result<WorldType, String> {
        let mut world = WorldType::new();
        //Iterateur sur Les lignes
        let mut input = input.lines();

        //* Gestion Premiére Line Xmax et Ymax

        let first_line = input.next().ok_or("Fichier vide ?")?;
        let mut line_espace_iterator = first_line.split_whitespace();

        world.coord_max.x = match line_espace_iterator
            .next()
            .ok_or("Premiere ligne vide ?")?
            .parse::<i32>()
        {
            Ok(x) => x,
            _ => return Err(format!("Erreur lors de la lecture de X_max")),
        };

        world.coord_max.y = match line_espace_iterator
            .next()
            .ok_or("Veuillez ecrire Y_max")?
            .parse::<i32>()
        {
            Ok(y) => y,
            _ => return Err(format!("Erreur lors de la lecture de Y_max")),
        };
        //Juste pour vérifier si les coordonnée sonts pas negatives
        if !is_in_world(&world.coord_max, &world.coord_max) {
            world.coord_max.clamp(&CoordType { x: 1, y: 1 });
        }
        //* Parsing des Nrobots et création des HashMap
        world.colored_case.push(HashMap::new());
        let mut id = 1;
        for i in input {
            //Creation d'un robot et link a une id
            let (mut robot, robot_coord) = RobotType::initiat(i, &id, &world.coord_max)?;
            match world.all_robots_coords.insert(robot_coord, id) {
                Some(_) => {
                    println!("Des robots possédent des coordonées d`apparition identique seul le dernier sera pris en compte");
                    continue;
                }
                None => (),
            };
            put_color(&mut robot, &mut world.used_color, id);
            robot.robot_print(&id);
            world.all_robots.insert(id, robot);
            world.colored_case[0].insert(robot_coord, id);
            id += 1;
        }
        Ok(world)
    }
    /// Fait bouger tout les robot jusqu'a la fin du jeu
    fn lancer(&mut self, color_lifetime: usize) -> Result<(), String> {
        let mut turn = 0;
        //Recuperation de toutes les id des robots
        let robot_id: Vec<u32> = self.all_robots.keys().cloned().collect();
        while !all_instruction_done(self) {
            //Creé un tour pour les couleurs
            self.colored_case.push(HashMap::new());
            //Applique un mouvement sur tout les robot
            for robot in robot_id.iter() {
                un_movement(
                    self.all_robots
                        .get_mut(robot)
                        .ok_or("Ce robot n`existe pas")?,
                    *robot,
                    &mut self.all_robots_coords,
                    turn,
                    &mut self.colored_case,
                    &self.coord_max,
                );
            }
            if turn < color_lifetime {
                turn += 1;
            } else {
                self.colored_case.remove(0);
            }
        }
        Ok(())
    }
}

//* Partie 1 du parsing
// !Si un type met de coms tout en haut plantage
/// Prends un fichier et le formate pour world.new()
fn string_for_world(filename: String, nb_rand_instruct: u32) -> Result<String, String> {
    // lis le fichier et retourne un vecteur des caractères lu en valeur ascii

    let contents = fs::read(filename).expect("Something went wrong reading the file");
    // Transcrit les valeurs ascii en char
    let contents = match str::from_utf8(&contents) {
        Ok(v) => v,
        Err(e) => return Err(e.to_string()),
    };
    // initialisation du vecteur comprenant les commandes a traiter
    let mut raw_commandes: String = String::new();
    // booléen empechant la lecture (utilisé dans la boucle for ci-dessous)
    let mut dont_read = false;
    // booléen pour signaler un passage de lettre a chiffre
    let mut is_letter = false;
    // booléen permettant de différencier juste une orientation d`une serie d`instructions complète
    let mut letter_length: u8 = 0;
    // compteur d`espace (utile pour push le premier retour a la ligne )
    let mut space: u8 = 0;

    // banque d`instructions
    const CHARSET: &[u8] = b"FLR";
    const ORIENT_SET: &str = "NSOE";
    // thread permettant les rng
    let mut rng = rand::thread_rng();
    // nbr d`instructions
    let len = rng.gen_range(0, nb_rand_instruct);
    // trie les commandes des commentaires
    for elem in contents.chars() {
        // arrète de lire lorsque elem est "/"
        if elem == '/' {
            dont_read = true;
        }
        // reprend à lire lorsque elem est un retour à la ligne, soit nouvelles instructions
        if elem == '\n' {
            dont_read = false;
        }
        // push une liste d`instruction aléatoire si non mentionné
        if letter_length == 1
            && is_letter == true
            && elem != 'L'
            && elem != 'R'
            && elem != 'F'
            && elem != '\n'
            && elem != ' '
            && dont_read == false
        {
            let instruct: String = (0..len)
                .map(|_| {
                    let idx = rng.gen_range(0, CHARSET.len());
                    CHARSET[idx] as char
                })
                .collect();
            raw_commandes.push_str(&instruct);
        }
        // passage de lettre a chiffre
        if elem >= 'E' && elem <= 'S' && dont_read == false {
            is_letter = true;
            letter_length += 1;
        }
        // compte les 2 premier espace
        if elem == ' ' && space <= 2 {
            space += 1
        }
        // push un retour a ligne au 2eme espace
        if elem == ' ' && space == 2 {
            raw_commandes.push('\n');
        }
        // Séparation entre chaque robots
        if (elem >= '0' && elem <= '9') && dont_read == false && is_letter == true {
            raw_commandes.push('\n');
            is_letter = false;
            letter_length = 0; // remet a 0 pour futur iteration
        }
        // push les carractère non commentarisé
        if dont_read == false && elem != '\n' {
            if !(elem == ' ' && is_letter == true) {
                raw_commandes.push(elem);
            }
        }
    }
    let instruct: String = (0..len)
        .map(|_| {
            let idx = rng.gen_range(0, CHARSET.len());
            CHARSET[idx] as char
        })
        .collect();
    let x = raw_commandes.pop();
    match x {
        Some(x) => {
            for t in ORIENT_SET.chars() {
                if t == x {
                    raw_commandes.push(x);
                    raw_commandes.push_str(&instruct);
                    return Ok(raw_commandes);
                }
            }
            raw_commandes.push(x);
            return Ok(raw_commandes);
        }
        None => return Ok(raw_commandes),
    }
}

/// Crée un fichier de commandes Fantôme aleatoire
fn rng_string_for_world(
    rng_max: CoordType,
    nb_instructions_max: u32,
    nb_robots_max: u32,
) -> String {
    // initialisation de la String d`initialisation du monde
    let mut instruct: String = String::new();
    // thread permettant les génération aléatoire
    let mut rng = rand::thread_rng();
    // initialisation des taille du monde
    let rng_xmax: i32 = rng.gen_range(1, rng_max.x);
    let rng_ymax: i32 = rng.gen_range(1, rng_max.y);
    // génération du nombree robots de manière aléatoire
    let mut i = rng.gen_range(1, nb_robots_max);
    // banque de coordonnées déjà utilisé
    let mut htable_coord: HashSet<CoordType> = HashSet::new();

    // banque d`instruction et d`orientation
    const CHARSET_I: &[u8] = b"FLR";
    const CHARSET_O: &[u8] = b"NSOE";

    // serie de push pour former la String
    instruct.push_str(&rng_xmax.to_string());
    instruct.push(' ');
    instruct.push_str(&rng_ymax.to_string());
    instruct.push('\n');
    // parti : initialisation des robots
    while i > 0 {
        // génération des coordonnées de départ du robot donné
        let rng_pos: CoordType = CoordType {
            x: rng.gen_range(0, rng_xmax),
            y: rng.gen_range(0, rng_ymax),
        };
        if !htable_coord.contains(&rng_pos) {
            htable_coord.insert(rng_pos);

            instruct.push_str(&rng_pos.x.to_string());
            instruct.push(' ');
            instruct.push_str(&rng_pos.y.to_string());
            instruct.push(' ');

            let idx_o = rng.gen_range(0, CHARSET_O.len());
            let orien = CHARSET_O[idx_o] as char;
            instruct.push(orien);

            // parti : création des instructions
            let j: u32 = rng.gen_range(0, nb_instructions_max);
            let rng_instruct: String = (0..j)
                .map(|_| {
                    let idx_i = rng.gen_range(0, CHARSET_I.len());
                    CHARSET_I[idx_i] as char
                })
                .collect();
            instruct.push_str(&rng_instruct);
            instruct.push('\n');
        }
        i -= 1
    }
    return instruct;
}

///Prends un robot et lui applique une instruction modifie en consequence les different champ d'un world
fn un_movement(
    robot: &mut RobotType,
    robot_id: u32,
    all_robots_coords: &mut HashMap<CoordType, u32>,
    turn: usize,
    colored_case: &mut Vec<HashMap<CoordType, u32>>,
    world_taille_max: &CoordType,
) {
    let robot_actual_coord = robot.coord;
    if !robot.instructions.is_empty() && robot.vivant {
        match robot.instructions[0] {
            InstructionType::Gauche => {
                robot.gauche();
                //Repose la couleur sur la case
                colored_case[turn].insert(robot_actual_coord, robot_id);
            }
            InstructionType::Droite => {
                robot.droite();
                //Repose la couleur sur la case
                colored_case[turn].insert(robot_actual_coord, robot_id);
            }
            InstructionType::Avancer => {
                let robot_advenced_coord = robot.avancer();
                if is_in_world(world_taille_max, &robot_advenced_coord) {
                    if !is_collision(all_robots_coords, &robot_advenced_coord) {
                        //Ce n'est pas une collision le mouvement est donc admis
                        move_in_robots_coords(
                            all_robots_coords,
                            &robot_advenced_coord,
                            &robot_actual_coord,
                        );
                        robot.coord = robot.avancer();

                        if is_in_enemy_colored_case(colored_case, &robot_advenced_coord, &robot_id)
                        {
                            //Le robot passe sur une couleur adverse il meurt
                            robot.vivant = false;
                        } else {
                            colored_case[turn].insert(robot_advenced_coord, robot_id);
                        }
                    } else {
                        println!(
                            "Robot ID<{}> Collision en ({}, {})",
                            robot_id, robot_advenced_coord.x, robot_advenced_coord.y
                        );
                    }
                }
            }
        }
        //Retrait de l'instruction suite a son execution
        robot.instructions.remove(0);
    }
}
//* Change les coords du robot dans la HashMap
fn move_in_robots_coords(
    all_robots_coords: &mut HashMap<CoordType, u32>,
    robot_advenced_coord: &CoordType,
    robot_old_coord: &CoordType,
) {
    match all_robots_coords.remove(robot_old_coord) {
        Some(x) => all_robots_coords.insert(*robot_advenced_coord, x),
        _ => None, // !Ne peux en theorie jamais arrivé car pas de robots a coordonée identiques
    };
}
//* Debut des boolean

//* Prends une position et verifie si collision
fn is_collision(robots_positions: &HashMap<CoordType, u32>, coord_test: &CoordType) -> bool {
    match robots_positions.get(coord_test) {
        Some(_) => return true,
        _ => return false,
    };
}
//* Prends une position et verifie si dans case une adverse
fn is_in_enemy_colored_case(
    world: &Vec<HashMap<CoordType, u32>>,
    robot_coord: &CoordType,
    robot_id: &u32,
) -> bool {
    for turn in world.iter() {
        match turn.get(robot_coord) {
            Some(x) => return x == robot_id,
            _ => continue,
        };
    }
    false
}
/// Prends une position et verifie si dans le monde
fn is_in_world(taille_max: &CoordType, coord_test: &CoordType) -> bool {
    !(coord_test.x > taille_max.x || coord_test.y > taille_max.y || coord_test.x < 0 || coord_test.y < 0)
}
/// Prends un monde et verifie si il est "fini"
fn all_instruction_done(world: &WorldType) -> bool {
    //? Placer les robots morts dans un seconde table de hashage pour diminuer les iterations ?
    for robot in world.all_robots.values() {
        if !robot.instructions.is_empty() && robot.vivant == true {
            return false;
        }
    }
    return true;
}


//* ========================== Fin boolean
/// Gen une couleur aleatoire et l'attribue a un robot
fn put_color(robot: &mut RobotType, used_color: &mut HashMap<ColorType, u32>, id: u32) {
    let mut rng = rand::thread_rng();
    let r = rng.gen_range(0, 255);
    let g = rng.gen_range(0, 255);
    let b = rng.gen_range(0, 255);
    let color = ColorType::new(r, g, b);

    match used_color.get(&color) {
        Some(_) => put_color(robot, used_color, id),
        None => {
            robot.color = color;
            used_color.insert(color, id);
        }
    }
}
///Affiche un monde
fn affichage(world: &WorldType) -> Result<(), String> {
    let max_x = world.coord_max.x;
    let print_x = world.coord_max.x;
    let max_y = world.coord_max.y;
    let mut print_y = world.coord_max.y;

    for iy in (0..=max_y).rev() {
        print!("{print_y:>width$} ", print_y = print_y, width = 6);
        for ix in 0..=max_x {
            let coord = CoordType { x: ix, y: iy };
            match world.all_robots_coords.get(&coord) {
                Some(x) => match world.all_robots.get(&x) {
                    Some(x) => print!("{}", x),
                    None => (),
                },
                None => {
                    let mut found = false;
                    for j in &world.colored_case {
                        match j.get(&coord) {
                            Some(x) => match world.all_robots.get(&x) {
                                Some(x) => {
                                    print!(" {icon:>width$} ", icon = "☁ ".truecolor(x.color.r, x.color.g, x.color.b), width = 1);
                                    found = true;
                                    break;
                                }
                                None => return Err("Bug Etrange".to_string()),
                            },
                            None => (),
                        }
                    }
                    if !found {
                        print!("[  ]")
                    }
                }
            }
        }
        print_y -= 1;
        println!();
    }
    print!("      ");
    for i in 0..=print_x {
        print!("{print_x:>width$}", print_x = i, width = 4);
    }
    println!("\n");
    Ok(())
}

fn menu() -> Result<(),String>{
    let instructions_file = string_for_world("commande.txt".to_string(),200)?;
    let rng_max = CoordType {
        x: 29,
        y: 100,
    };
    let instructions = rng_string_for_world(rng_max, 100, 100);

    let mut world:WorldType;
        let mut buffer = String::new();
        let flux = io::stdin(); 
        println!("{}","======================== Let`s Torture Dance Start'uh =========================\n".red());
        println!("{}","      Choose your Mode :".yellow());
        println!("{}","           1:File Mode".blue());
        println!("{}","           2:Rng  Mode".blue());
        println!("{}","           3:Mode Kéké".cyan());
        match flux.read_line(&mut buffer){
            Ok(_) => match buffer.chars().next().ok_or("Veuillez Choisir une action")? {
                '1' =>  world = WorldType::initiate(instructions_file)?,
                '2' =>  world = WorldType::initiate(instructions)?,
                '3' => world = WorldType::initiate(rng_string_for_world(CoordType{x:10000000,y:10000000}, 100, 1000000))?,
                 _ => process::exit(1),
            },
            _ => return Err("Erreur pendant le menu du jeu".to_string()),
        };
        println!();
        println!("World {}",world.coord_max);
        println!();
        println!("Etat initial\n============================");
        if is_in_world(&rng_max, &world.coord_max){
            affichage(&world)?;
            world.lancer(5)?;
            affichage(&world)?;
        }
        else {
            world.lancer(5)?;
            println!("Monde trop grand pour un affichage stable");
        }
        
        println!("Etat final\n============================");
        Ok(())
}


fn main() -> Result<(), String> {
    menu()?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test() -> Result<(), String> {
        let filename = "test_file.txt".to_string();
        let instructions_file = string_for_world(filename, 100)?;
        let mut world = WorldType::initiate(instructions_file)?;
        world.lancer(5)?;
        Ok(())
    }

    #[test]
    fn test_string_for_world() -> () {
        let filename = String::from("commande.txt");
        match string_for_world(filename, 10) {
            Ok(x) => println!("{}", x),
            Err(_) => println!("Fail lecture fichier"),
        }
    }

    #[test]
    fn test_rng_string_for_world() -> () {
        let taille_max = CoordType{x: 20, y: 20};
        println!("{}", rng_string_for_world(taille_max, 10, 10))
    }

    #[test]
    fn test_put_color() {
        let robot: &mut RobotType = &mut RobotType::new();
        let used_color: &mut HashMap<ColorType, u32> = &mut HashMap::new();
        let id = 1;
        put_color(robot, used_color, id);
        println!("{x}", x = "robot color".truecolor(robot.color.r, robot.color.g, robot.color.b))
    }
}

//TODO Display,Récrire des tests adapter,Gestion obstacles
